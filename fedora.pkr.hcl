packer {
  required_plugins {
    virtualbox = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/virtualbox"
    }
    sshkey = {
      version = ">= 1.0.1"
      source  = "github.com/ivoronin/sshkey"
    }
  }
}

data "sshkey" "install" {
}

source "virtualbox-iso" "devbox" {
  boot_command     = ["<enter>", "<wait10m>"]
  boot_wait        = "5s"
  iso_checksum     = "sha256:${var.iso_checksum}"
  iso_url          = "${var.iso_url}"
  guest_os_type    = "Fedora_64"
  ssh_username     = "${var.username}"
  ssh_password     = "${var.password}"
  ssh_timeout      = "20m"
  cpus             = 2
  memory           = 2048
  disk_size        = 20480
  keep_registered  = true
  shutdown_command = "echo '${var.password}' | sudo -S /sbin/shutdown -P -h now"
  vboxmanage = [
    [
      "modifyvm",
      "{{.Name}}",
      "--clipboard",
      "bidirectional"
    ],
    [
      "modifyvm",
      "{{.Name}}",
      "--draganddrop",
      "bidirectional"
    ]
  ]
}

build {
  sources = ["sources.virtualbox-iso.devbox"]

  # Provisioners
  provisioner "shell" {
    inline = [
      "echo '${var.password}' | sudo -S dnf update -y",
      "echo '${var.password}' | sudo -S dnf install ansible -y"
    ]
  }
  provisioner "ansible-local" {
    command        = "echo '${var.password}' | sudo -S ansible-playbook"
    playbook_dir   = "./ansible"
    playbook_file  = "./ansible/playbook.yml"
    galaxy_command = "echo '${var.password}' | sudo -S ansible-galaxy"
    galaxy_file    = "./ansible/requirements.yml"
    extra_arguments = [
      "--become",
      "--become-method=sudo",
      "--become-user=root"
    ]
  }
  provisioner "shell" {
    inline = [
      "git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/plugins/zsh-syntax-highlighting",
      "echo 'export PATH=$PATH:/usr/local/go/bin' >> ~/.zshrc"
    ]
  }

  # Post-Processors
  post-processor "vagrant" {
    compression_level = 9
    output            = "fedora-devbox.box"
  }
  post-processor "manifest" {
    output     = "manifest.json"
    strip_path = true
  }
  post-processor "checksum" {
    checksum_types = ["sha256"]
    output         = "packer_{{.BuildName}}_{{.ChecksumType}}.checksum"
  }
}
