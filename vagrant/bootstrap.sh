#!/usr/bin/env bash

dnf -y upgrade

dnf -y update

dnf -y install ansible

cd /vm_provisioning
cat vars.yml
ansible-galaxy install -r requirements.yml
ansible-playbook --connection=local -i inventory playbook.yml --extra-vars "user_das=$1"

sudo systemctl restart NetworkManager-wait-online.service

echo "Hello VM!"
