## Requirements

### Virtualbox
[VirtualBox](https://www.virtualbox.org/) is an open source virtualizer, an application that can run an entire operating system within its own virtual machine.<br>
Tested with version >= 6.x

1. Download the installer for your laptop operating system using the links below.
    * [VirtualBox download](https://www.virtualbox.org/wiki/Downloads)
    * or Company Portal if you encounter admin issues
1. Reboot your laptop if prompted to do so when installation completes.

### Vagrant
[Vagrant](https://www.vagrantup.com/) is an open source command line utility for managing reproducible developer environments.<br>
Stable version >= 2.2.0

1. Download the installer for your laptop operating system using the links below.
    * [Vagrant download](https://www.vagrantup.com/downloads)
    * or Company Portal if you encounter admin issues
1. Reboot your laptop if prompted to do so when installation completes.

## What's included ?

- curl
- git
- docker
- docker-compose
- terraform
- Openshift CLI (v3)
- fish/starship
- sdkman (java, maven pre-installed)
- golang :D

## Setup

1. Install VirtualBox & Vagrant
2. Replace DAS with your DAS and SHARED_FOLDER with a folder you want to share in the VM then run : `DAS='a677474' SHARED_FOLDER='...' vagrant up`
    - Take a coffee break
3. Login : `ssh tpdops@localhost -p $(vagrant ssh-config devbox | grep -i port | awk '{print $2}')`
    - user by default : `tpdops`
    - password by default : `password`
    - you can change default values : `ansible/vars.yml` (vm_username & vm_password)

PS: shared folder is in /Shared in the guest machine
